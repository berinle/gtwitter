package org.example.demo

import twitter4j.Twitter
import twitter4j.TwitterFactory
import twitter4j.auth.RequestToken

import javax.servlet.ServletException

class TwitterController {

    def auth() {
        def twitter = new TwitterFactory().instance

        def key = grailsApplication.config.grails.plugins.springsecurity.twitter.app.consumerKey
        def secret = grailsApplication.config.grails.plugins.springsecurity.twitter.app.consumerSecret
        twitter.setOAuthConsumer(key, secret)
        request.session.setAttribute "twitter", twitter

        def callbackURL = request.requestURL
        println "$request.remoteAddr | $request.remoteHost | $request.requestURI"
        println "request.contextPath => ${request.contextPath}"
        println "(before) callbackURL => ${callbackURL}"
//        def index = callbackURL.indexOf(request.contextPath) + request.contextPath.length()
        def index = callbackURL.indexOf("/gtwitter") + request.contextPath.length()
        callbackURL.replace(index, callbackURL.length(), "").append("/callback")
        println "(after) callbackURL => ${callbackURL}"

        try {
            def requestToken = twitter.getOAuthRequestToken(callbackURL.toString())
            println "### $requestToken.authenticationURL"
            request.session.setAttribute "requestToken", requestToken
            response.sendRedirect requestToken.authenticationURL
        } catch (e) {
            throw new ServletException(e)
        }
    }

    def callback(){
        Twitter twitter = request.session.getAttribute("twitter") as Twitter
        println "twitter => $twitter"
        RequestToken requestToken = request.session.getAttribute("requestToken") as RequestToken
        println "request token => $requestToken"
        String verifier = request.getParameter("oauth_verifier")
        println "verifier => $verifier"

        try {
            if (verifier != null){
                twitter.getOAuthAccessToken(requestToken, verifier)
            } else {
                request.session.removeAttribute 'twitter'
            }
            request.session.removeAttribute "requestToken"

        } catch (e) {
            throw new ServletException(e);
        }

        redirect(controller: "home")
//        response.sendRedirect(request.getContextPath() + "/");
    }

    def logout() {
        request.session.invalidate()
        redirect(controller: "home")
    }
}
