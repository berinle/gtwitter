import com.the6hours.grails.springsecurity.twitter.TwitterUserDomain
import org.example.demo.SecUser

class TwitterUser implements TwitterUserDomain {

    int uid
    String screenName
    String tokenSecret
    String token

	static belongsTo = [user: SecUser]

	static constraints = {
		uid unique: true
	}
}
