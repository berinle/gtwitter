<html>
<head>
	
	<title>Home : Welcome</title>
</head>

<body>
	<twitterAuth:button/>

    <br>

    <g:if test="${session.getAttribute('twitter') != null}">
        Welcome ${session.getAttribute('twitter').screenName}
        <g:link controller="twitter" action="logout">Sign out</g:link>
    </g:if>
    <g:else>
        <g:link controller="twitter" action="auth">Sign in with Twitter</g:link>
    </g:else>


</body>