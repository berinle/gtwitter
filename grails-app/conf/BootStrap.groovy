import org.example.demo.*

class BootStrap {

    def init = { servletContext ->

    	def userRole = SecRole.findByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
        def adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)

        def adminUser = SecUser.findByUsername('admin') ?: new SecUser(
                username: 'admin',
                password: 'pwd',
                enabled: true).save(failOnError: true)

        if (!adminUser.authorities.contains(adminRole)) {
            SecUserSecRole.create adminUser, adminRole
        }

        def user = SecUser.findByUsername('john') ?: new SecUser(
                username: 'john',
                password: 'pwd',
                enabled: true
        ).save(failOnError: true)

        if(!user.authorities.contains(userRole)){
            SecUserSecRole.create user, userRole
        }

        println "done creating user and roles"

    }
    def destroy = {
    }
}
