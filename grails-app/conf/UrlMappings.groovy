class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller:'home')
		"/callback"(controller:'twitter', action: 'callback')
		"/logout"(controller:'twitter', action: 'logout')
		"500"(view:'/error')
	}
}
